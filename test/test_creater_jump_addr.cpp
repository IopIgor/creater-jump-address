#include <systemc.h>
#include <iostream>
#include "creater_jump_addr.hpp"

using namespace std;
using namespace sc_core;

const sc_time wait_time(1, SC_NS);
const unsigned ele_test = 5;
  //theoretical values calculated as 2^28*pc4+instr
const sc_bv<32> ris[ele_test] = {5, 15, 536870927, 536870917, 536870932};

SC_MODULE(TestBench) 
{
 public:
  sc_signal<sc_bv<26> > instr;
  sc_signal<sc_bv<4> > pc4;
  sc_signal<sc_bv<32> > jump_addr;

  creater_jump_addr jump_test;

  SC_CTOR(TestBench) : jump_test("jump_test")
  {
    SC_THREAD(stimulus_thread);
    SC_THREAD(check_and_watcher);
      sensitive << instr << pc4;

    jump_test.instr(this->instr);
    jump_test.pc4(this->pc4);
    jump_test.jump_addr(this->jump_addr);
  }

  bool Check() const {return error;}
  
 private:
  bool error;

  void stimulus_thread() 
  {
    pc4.write(0);
    instr.write(5);
    wait(wait_time);

    instr.write(15);
    wait(wait_time);

    pc4.write(2);
    wait(wait_time);

    instr.write(5);
    wait(wait_time);

    instr.write(20);
    wait(wait_time);
  }

  void check_and_watcher() 
  {
    error = 0;
    unsigned i = 0;
    while(i < ele_test)
    {
      wait();
      wait(1, SC_PS); //to avoid the execution of "if" condition before execution of the component
      cout << "At time " << sc_time_stamp() << ": Created jump_address " << jump_addr.read() 
           << endl;
      if(jump_addr.read() != ris[i])
      {
        cout << "test failed!!\nTheoretical value: " << ris[i]
             << "\nCreated value: " << jump_addr.read() << endl << endl;
        error = 1;
      }
      i++;
    }
  }
};

int sc_main(int argc, char** argv) 
{
  TestBench test("test");

  sc_start();

  return test.Check();
}

