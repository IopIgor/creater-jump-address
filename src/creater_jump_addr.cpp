#include <systemc.h>
#include "creater_jump_addr.hpp"

using namespace std;
using namespace sc_core;

void creater_jump_addr::behav()
{
    while(true)
    {
      wait();
      jump_addr->write((pc4->read(),"00",instr->read()));
    }
}
