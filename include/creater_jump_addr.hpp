#ifndef creater_jump_addr_HPP
#define creater_jump_addr_HPP

#include <systemc.h>
#include <iostream>

using namespace std;
using namespace sc_core;

SC_MODULE(creater_jump_addr) 
{
  sc_in<sc_bv<26> > instr;
  sc_in<sc_bv<4> > pc4;
  sc_out<sc_bv<32> > jump_addr;
  
  SC_CTOR(creater_jump_addr) 
  {
    SC_THREAD(behav);
        sensitive << instr << pc4;
  } 
  
 private:
  void behav();
};

#endif
